app.models.topic = {
    create: function(topic, callback) {
        var dataString = 'name=' + encodeURIComponent(topic.name) +
                         '&parent=' + encodeURIComponent(topic.parent) +
                         '&subtopic=' + encodeURIComponent(topic.subtopic) +
                         '&session_key=' + app.controllers.users.getSession(),
            result;
        x$('.received').xhr('http://api.example.org/topic/create', {
            method: 'POST',
            data: dataString,
            async: true,
            error: function() {
                navigator.notification.alert("Klaida");
                callback(result);
            },
            callback: function() {
                response = JSON.parse(this.responseText);
                if (response.success === "TOPIC_CREATED") {
                    result = true;
                } else {
                    result = response.error;
                }
                callback(result);
            }
        });
    },

    getTopics: function(callback) {
        return this.getTopic(0, callback);
    },

    getTopic: function(parent, callback) {
        var result;
        x$('.received').xhr('http://api.example.org/topic/with_parent/' + parent, {
            method: 'GET',
            async: true,
            error: function() {
                navigator.notification.alert("Klaida");
                callback(result);
            },
            callback: function() {
                response = JSON.parse(this.responseText);
                result = response.topics;
                callback(result);
            }
        });
    }
};
