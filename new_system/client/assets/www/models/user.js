app.models.user = {
    login: function(username, password, callback) {
        var dataString = 'username=' + encodeURIComponent(username) +
            '&password=' + encodeURIComponent(password),
            result;
        x$('.received').xhr('http://api.example.org/user/login', {
            method: 'POST',
            data: dataString,
            async: true,
            error: function() {
                navigator.notification.alert("Klaida");
                callback(result);
            },
            callback: function() {
                response = JSON.parse(this.responseText);
                console.log('User logged in with: ' + this.responseText);
                if (response.success == "USER_LOGGED_IN") {
                    callback({key: response.session_key, role: response.role});
                }
                else {
                    callback(false);
                }
            }
        });
    },

    create: function(user, callback) {
        var dataString = 'firstname=' + encodeURIComponent(user.firstname) +
                         '&lastname=' + encodeURIComponent(user.lastname) +
                         '&username=' + encodeURIComponent(user.username) +
                         '&email=' + encodeURIComponent(user.email) +
                         '&role=' + encodeURIComponent(user.role),
            result;
        x$('.received').xhr('http://api.example.org/user/create', {
            method: 'POST',
            data: dataString,
            async: true,
            error: function() {
                navigator.notification.alert("Klaida");
                callback(result);
            },
            callback: function() {
                response = JSON.parse(this.responseText);
                if (response.success == "USER_CREATED") {
                    result = {success: true, password: response.password};
                } else {
                    result = {success: false, error: response.error};
                }
                callback(result);
            }
        });
    }
};
