app.models.publicMessage = {
    getMessages: function(topic, since, callback) {
        var result,
            dataString = 'topic=' + encodeURIComponent(topic) +
                         '&since=' + since;
        x$('.received').xhr('http://api.example.org/publicmessage/get_new/', {
            method: 'POST',
            data: dataString,
            async: true,
            error: function() {
                navigator.notification.alert("Klaida");
                callback(result);
            },
            callback: function() {
                response = JSON.parse(this.responseText);
                result = response.messages;
                callback(result);
            }
        });
    },

    sendPublicMessage: function(topic, message, callback) {
        var result,
            dataString = 'topic=' + encodeURIComponent(topic) +
                         '&message=' + encodeURIComponent(message) +
                         '&session_key=' + app.controllers.users.getSession();
        x$('.received').xhr('http://api.example.org/publicmessage/create/', {
            method: 'POST',
            data: dataString,
            async: true,
            error: function() {
                navigator.notification.alert("Klaida");
                callback(result);
            },
            callback: function() {
                response = JSON.parse(this.responseText);
                if (response.success === "MESSAGE_POSTED") {
                    result = true;
                } else {
                    result = response.error;
                }
                callback(result);
            }
        });
    }
};
