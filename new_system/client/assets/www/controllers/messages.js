app.controllers.messages = {
    navigateWindow: function(window_name, data){
        app.windowsStack.push({
            controller: this,
            window_name: window_name,
            data: data
        });
        app.showSpinner();
        switch(window_name) {
            case "WRITE_PUBLIC_MESSAGE":
                app.clearInterval();
                app.loadView('views/writePublicMessage.html');
                x$('#topic').attr('value', data.topic);
                x$('#parent_name').attr('value', data.parent_name);
                app.hideSpinner();
                break;

            case "VIEW_MESSAGES":
                if (typeof(data) !== "undefined") {
                    parent = data.parent;
                }
                var parent_name = '';
                if (typeof(data.parent_name) !== "undefined") {
                    parent_name = data.parent_name.toString();
                }
                app.loadView('views/messagesWindow.html');
                x$('#topic-id').attr('value', parent);
                x$('h2').html(parent_name);


                if (typeof(app.controllers.topics.session) === 'undefined' || app.controllers.topics.session.topic !== parent) {
                    app.controllers.topics.session = {start: Math.round(new Date().getTime() / 1000 - 24 * 60 * 60), topic: parent };
                }

                app.models.publicMessage.getMessages(parent, app.controllers.topics.session.start, function(messages) {
                    if (messages.length !== 0) {
                        app.controllers.messages.lastCheck = messages[messages.length-1].timestamp;
                    }
                    else {
                        app.controllers.messages.lastCheck = app.controllers.topics.session.start;
                    }
                    var messagesContent = x$('ul.messages');
                    for (i = 0; i < messages.length; i++) {
                        console.log('base:' + messages[i].message);
                        datetime = messages[i].timestamp * 1000;
                        messagesContent.html('bottom', "<li><span class='datetime'>[" +
                                                     app.controllers.messages.formatDate(datetime) +
                                                    "]</span><span class='author'>" +
                                                    messages[i].author.firstname +
                                                    " " + messages[i].author.lastname +
                                                    "</span><div class='clearifx'></div><div class='message'>" + messages[i].message + "</div></li>");
                    }


                    interval = window.setInterval(function(){app.controllers.messages.checkForUpdates(parent);}, 5 * 1000); //5 seconds
                    app.addInterval(interval);
                    app.scrollToBottom();
                    app.hideSpinner();
                });
                break;

        }
    },

    sendPublicMessage: function(topic, message) {
        if (app.controllers.messages.isEmpty(message)) {
            navigator.notification.alert("Tuščia žinutė");
        }
        app.showSpinner();
        app.models.publicMessage.sendPublicMessage(topic.topic, message, function(response) {
            if (response === true) {
                navigator.notification.alert("Žinutė išsiųsta");
                app.controllers.messages.navigateWindow('VIEW_MESSAGES',{'parent':topic.topic, 'parent_name': topic.parent_name});
            } else {
                switch(response){
                    case "EMPTY_TOPIC_AND_MESSAGE":
                        navigator.notification.alert("Tuščia žinutė");
                        break;
                    case "INVALID_SESSION_KEY":
                        app.controllers.users.deleteSession();
                        app.controllers.users.navigateWindow("LOGIN");
                        break;
                    case "TOPIC_DOES_NOT_EXIST":
                        navigator.notification.alert("Temos nėra");
                        break;
                }
            }
            app.hideSpinner();
        });
    },

    isEmpty: function(message) {
        return message.length === 0;
    },

    checkForUpdates: function(topic) {
        var messages = [];

        lastTime = this.lastCheck;
        app.controllers.messages.lastCheck = Math.round(new Date().getTime() / 1000);

        app.models.publicMessage.getMessages(topic, lastTime, function(messages) {
            messagesContent = x$('ul.messages');
            for (i = 0; i < messages.length; i++) {
                console.log('check:' + messages[i].message);
                datetime = messages[i].timestamp * 1000;
                messagesContent.html('bottom', "<li><span class='datetime'>[" +
                                             app.controllers.messages.formatDate(datetime) +
                                            "]</span><span class='author'>" +
                                            messages[i].author.firstname +
                                            " " + messages[i].author.lastname +
                                            "</span><div class='clearifx'></div><div class='message'>" + messages[i].message + "</div></li>");
            }
            if (messages.length > 0) {
                app.scrollToBottom();
            }
        });
    },
    formatDate: function(d){
        if(typeof d === 'number') { d = new Date(d);}
        if(!(d instanceof Date)) { return d; }
        function pad(n){return n<10 ? '0'+n : n;}
        return pad(d.getFullYear()) + '-' +
            pad(d.getMonth()) + '-' +
            pad(d.getDate()) + " " +
            pad(d.getHours()) + ":" +
            pad(d.getMinutes()) + ":" +
            pad(d.getSeconds());
    }
};
