app.controllers.users = {
    navigateWindow: function(window_name, data){
        app.windowsStack.push({
            controller: this,
            window_name: window_name,
            data: data
        });
        switch(window_name) {
            case "CREATE_USER":
                app.loadView('views/createUser.html');
                break;
            case "DELETE_USER":
                app.loadView('views/deleteUser.html');
                break;
            case "LOGIN":
                app.loadView('views/login.html');
                break;
            case "USER_MAIN":
                app.loadView('views/userMain.html');
                break;
            case "EDITOR_MAIN":
                app.loadView('views/editorMain.html');
                break;
            case "ADMIN_MAIN":
                app.loadView('views/adminMain.html');
                break;
        }
    },

    login: function(username, password) {
        app.showSpinner();
        app.models.user.login(username, password, function (user) {
            if (user !== false) {
                app.controllers.users.saveSession(user.key);
                app.controllers.users.saveUser(user);
                if(user.role === '0') {
                    app.controllers.users.navigateWindow("USER_MAIN");
                } else if (user.role === '1') {
                    app.controllers.users.navigateWindow("EDITOR_MAIN");
                } else if (user.role === '2') {
                    app.controllers.users.navigateWindow("ADMIN_MAIN");
                }
            } else {
                navigator.notification.alert("Vartotojas tokiais duomenimis neegzistuoja. Pasitikrinkite vartotojo vardą ir slaptažodį");
            }
            app.hideSpinner();
        });
    },

    saveSession: function(key) {
        var date = new Date();
        date.setTime(date.getTime() + (10 * 24 * 60 * 60 * 1000)); // Expires in 10 days
        this.session = {
            key: key,
            expires: date.toGMTString()
        };
    },

    getSession: function() {
        if (typeof this.session !== 'undefined') {
            ret = this.session.key;
        } else {
            ret = null;
        }
        return ret;
    },
    deleteSession: function() {
        this.session = {};
    },

    saveUser: function(user) {
        this.current_user = user;
    },

    getCurrentUser: function(user) {
        if (typeof this.current_user !== 'undefined') {
            ret = this.current_user;
        } else {
            ret = null;
        }
        return ret;
    },


    logout: function(){
        this.deleteSession();
        this.navigateWindow("LOGIN");
    },
    create: function(user) {
        app.showSpinner();
        app.models.user.create(user, function(response) {
            if (response.success === true) {
                navigator.notification.alert("Paskyra sėkmingai sukurta. Slaptažodis:" + response.password);
                app.controllers.users.navigateWindow("ADMIN_MAIN");
            } else if (response === 'USERNAME_ALREADY_TAKEN'){
                navigator.notification.alert("Tokia paskyra jau egzistuoja");
            }
            app.hideSpinner();
        });
    }

};
