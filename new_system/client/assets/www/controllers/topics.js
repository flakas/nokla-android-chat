app.controllers.topics = {
    navigateWindow: function(window_name, data){
        var topics,
            parent = 0,
            parent_name = '',
            topicsContent,
            messages,
            messagesContent,
            i;
        app.windowsStack.push({
            controller: this,
            window_name: window_name,
            data: data
        });
        app.showSpinner();
        switch(window_name) {
            case "TOPICS_WINDOW":
                app.models.topic.getTopics(function(topics) {
                    var content = '';
                    for (i = 0; i < topics.length; i++) {
                        content += "<li class='priority-"+topics[i].priority+"'><a href='#' class='topic' id='topic-" + topics[i].id + "'>" + topics[i].name + "</a></li>";
                    }
                    if (topics.length === 0) {
                        content = "Temų nėra";
                    }

                    app.loadView('views/topicsWindow.html');
                    x$('ul.topics').html(content);

                    x$('a.topic').click(function(e){
                        e.preventDefault();
                        var id = x$(this).attr('id')[0].substring('topic-'.length);
                        var name = x$(this).html();
                        app.controllers.topics.navigateWindow('TOPIC_WINDOW', {'parent':id, 'parent_name': name.toString()});
                    });
                    app.hideSpinner();
                });
                break;
            case "TOPIC_WINDOW":
                if (typeof(data) !== "undefined") {
                    parent = data.parent;
                }
                if (typeof(data.parent_name) !== "undefined") {
                    parent_name = data.parent_name;
                }

                app.models.topic.getTopic(parent, function(topics) {
                    topicsContent = '';
                    for (i = 0; i < topics.length; i++) {
                        if (topics[i].subtopic === '1') {
                            topicsContent += "<li class='priority-"+topics[i].priority+"'><a href='#' class='subtopic' id='subtopic-"+topics[i].id + "'>" + topics[i].name + "</a></li>";
                        } else {
                            topicsContent += "<li class='priority-"+topics[i].priority+"'><a href='#' class='topic' id='topic-"+topics[i].id + "'>" + topics[i].name + "</a></li>";
                        }
                    }
                    if (topics.length === 0) {
                        topicsContent = "Temų nėra";
                    }

                    var role = app.controllers.users.getCurrentUser().role;
                    if (role === '0') {
                        app.loadView('views/userTopicWindow.html');
                    }
                    else if (role === '1') {
                        app.loadView('views/redTopicWindow.html');
                    }

                    x$('ul.subtopics').html(topicsContent);
                    x$('#topic-id').attr('value', parent);
                    x$('h2').html(parent_name);

                    x$('a.topic').click(function(e){
                        e.preventDefault();
                        var id = x$(this).attr('id')[0].substring('topic-'.length);
                        var name = x$(this).html();
                        app.controllers.topics.navigateWindow('TOPIC_WINDOW', {'parent':id, 'parent_name': name.toString()});
                    });
                    x$('a.subtopic').click(function(e){
                        e.preventDefault();
                        var id = x$(this).attr('id')[0].substring('subtopic-'.length);
                        var name = x$(this).html();
                        app.controllers.topics.navigateWindow('SUBTOPIC_WINDOW', {'parent':id, 'parent_name': name.toString()});
                    });
                    app.hideSpinner();
                 });
                break;

            case "SUBTOPIC_WINDOW":
                if (typeof(data) !== "undefined") {
                    parent = data.parent;
                }
                if (typeof(data.parent_name) !== "undefined") {
                    parent_name = data.parent_name;
                }

                var role = app.controllers.users.getCurrentUser().role;
                if (role === '0') {
                    app.loadView('views/userSubTopicWindow.html');
                }
                else if (role === '1') {
                    app.loadView('views/redSubTopicWindow.html');
                }

                x$('#topic-id').attr('value', parent);
                x$('h2').html(parent_name);

                app.hideSpinner();
                break;


            case "CREATE_TOPIC":
                var current = 0;
                if (typeof(data) !== "undefined") {
                    current = data.topic;
                }
                app.loadView('views/createTopic.html');
                x$('#parent').attr('value', current);
                app.hideSpinner();
                break;

        }
    },
    create: function(topic) {
        if(typeof(topic.parent) === "undefined") { topic.parent = 0; }
        if (topic.name.length === 0) {
            navigator.notification.alert("Nurodykite temos pavadinimą");
        }
        app.showSpinner();
        app.models.topic.create(topic, function(response) {
            if (response === true) {
                navigator.notification.alert("Tema sėkmingai sukurta");
                app.controllers.topics.navigateWindow('TOPIC_WINDOW', {parent: topic.parent});
            } else {
                switch(response){
                    case "EMPTY_TOPIC_NAME":
                        navigator.notification.alert("Nurodykite temos pavadinimą");
                        break;
                    case "INVALID_SESSION_KEY":
                        app.controllers.users.deleteSession();
                        app.controllers.users.navigateWindow("LOGIN");
                        break;
                    case "TOPIC_ALREADY_EXISTS":
                        navigator.notification.alert("Temos vardas yra užimtas");
                        break;
                }
            }
            app.hideSpinner();
        });
    }
};
