/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    models: {},
    controllers: {},
    // Application Constructor
    initialize: function() {
        this.windowsStack = [];
        app.controllers.users.navigateWindow('LOGIN');
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        document.addEventListener('backbutton', app.onBackButton, false);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
    },
    loadView: function(url, backing) {
        x$('.received').xhr(url, {
            method: "GET",
            async: false,
            error: function() {
                navigator.notification.alert("Klaida");
            },
            callback: function() {
                x$('.content').html(this.responseText);
            }
        });
    },
    onBackButton: function() {
        app.windowsStack.pop();
        backing = app.windowsStack.pop();
        if (typeof backing === 'undefined') {
            navigator.app.exitApp();
        } else {
            backing.controller.navigateWindow(backing.window_name, backing.data);
        }
    },
    addInterval: function(interval) {
        this.clearInterval();
        this.interval = interval;
    },
    clearInterval: function() {
        console.log('clearing');
        if (typeof(this.interval) !== 'undefined') {
            window.clearInterval(this.interval);
        }
    },
    showSpinner: function() {
        x$('.spinner').addClass('showSpinner');
        console.log('Showing spinner, current style: ' + x$('.spinner').attr('class'));
    },
    hideSpinner: function() {
        x$('.spinner').removeClass('showSpinner');
        console.log('Hiding spinner, current style: ' + x$('.spinner').attr('class'));
    },
    scrollToBottom: function() {
        // Scroll to the bottom of the page
        window.scrollTo(0, document.body.scrollHeight);
    },
    isAtScrollBottom: function() {
        return document.scrollTop === document.scrollHeight - document.clientHeight;
    }
};
