<?php

define('BASEPATH', '/var/www/pokprog/');
define('APPPATH', BASEPATH . 'application/');
define('SYSTEMPATH', BASEPATH . 'core/');

require SYSTEMPATH . 'Common.php';
require SYSTEMPATH . 'Core.php';

Core::getInstance()->run();
