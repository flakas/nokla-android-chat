<?php

require(SYSTEMPATH . 'Load.php');

class Core {

    public $load;
    public static $core;

    public static function getInstance() {
        if (!Core::$core) {
            Core::$core = new Core();
        }
        return Core::$core;
    }

    public function run() {
        $this->load->initialize();
        $this->router->route();
    }

    protected function __construct() {
        $this->load = new Load();
    }

}
