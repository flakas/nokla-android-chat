<?php

class Topic extends Model {

    function isTopicValid($name, $parentID, $subtopic) {
        $parentID = (int) $parentID;
        $query = $this->db->querySafe(
            "SELECT * FROM topics WHERE id = '%d'",
            array($parentID)
        );

        if ($this->db->numRows($query) >= 1) {
            $parent = $this->db->fetchSingle($query);
            if ($parent['name'] == $name) {
                return false;
            }
        } elseif ($parentID != 0) {
            return false;
        }

        $query = $this->db->querySafe(
            "SELECT * FROM topics WHERE parent = '%d' AND name = '%s'",
            array($parentID, $name)
        );

        if ($this->db->numRows($query) == 0) {
            return true;
        } else {
            return false;
        }
    }

    function createTopic($topic){
        $query = $this->db->querySafe(
            "INSERT INTO topics (name, parent, author, subtopic, priority) VALUES ( '%s', '%s', '%s', '%s', '%s')",
            array(
                $topic['name'], 
                $topic['parent'], 
                $topic['author']['id'], 
                $topic['subtopic'], 
                $topic['priority']
            )
        );
        return (boolean) $query;
    }

    function exists($id) {
        return (boolean) $this->db->querySafe(
            "SELECT * FROM topics WHERE id='%d'",
            array((int) $id)
        );
    }

    function getListByParent($id) {
        $query = $this->db->querySafe(
            "SELECT * FROM topics WHERE parent='%d' ORDER BY id ASC",
            array((int) $id)
        );
        if ($this->db->numRows($query)) {
            return $this->db->fetchAll($query);
        } else {
            return array();
        }
    }
}
