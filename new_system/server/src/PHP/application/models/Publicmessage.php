<?php

class Publicmessage extends Model {
    const NEW_MESSAGE_LIMIT = 50;

    function create($message) {
        $query = $this->db->querySafe(
            "INSERT INTO public_messages (author, topic, message, timestamp) VALUES ('%s', '%s', '%s', '%d')",
            array($message['author']['id'], $message['topic'], $message['message'], time())

        );
        return (boolean) $query;
    }

    function getNew($topic, $since = 0) {
        $since = (int) $since;
        $query = $this->db->querySafe(
            "SELECT * FROM (SELECT t1.*, t2.firstname, t2.lastname, t2.username, t2.role FROM public_messages AS t1, users AS t2 WHERE t1.author=t2.id AND topic='%d' AND timestamp>'%d' ORDER BY timestamp DESC LIMIT %d) AS t1 ORDER BY t1.timestamp ASC",
            array((int) $topic, $since, self::NEW_MESSAGE_LIMIT)
        );
        if ($this->db->numRows($query)) {
            $results = $this->db->fetchAll($query);
            $formattedResults = array();
            foreach ($results as $result) {
                $formattedResults[] = array (
                    'topic' => $result['topic'],
                    'message' => $result['message'],
                    'timestamp' => $result['timestamp'],
                    'author' => array(
                        'firstname' => $result['firstname'],
                        'lastname' => $result['lastname'],
                        'username' => $result['username']
                    )
                );
            }
            return $formattedResults;
        } else {
            return array();
        }
    }
}
