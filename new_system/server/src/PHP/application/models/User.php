<?php

class User extends Model {
    const PASSWORD_LENGTH = 10;
    const SESSION_KEY_EXPIRES_IN = 864000; // 10 days

    function userExists($username, $password = NULL) {
        if ($password == NULL) {
            $query = $this->db->querySafe(
                "SELECT * FROM users WHERE username='%s'",
                array($username)
            );
        } else {
            $query = $this->db->querySafe(
                "SELECT * FROM users WHERE username='%s' AND password='%s'",
                array($username, $this->encodePassword($password))
            );
        }
        return $this->db->numRows($query) == 1;
    }

    function createUser($user) {
        $password = $this->generateRandomPassword(self::PASSWORD_LENGTH);
        $user['password'] = $this->encodePassword($password);
        $query = $this->db->querySafe(
            "INSERT INTO users (firstname, lastname, username, email, role, password) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
            $user
        );
        echo $this->db->error();
        return $password;
    }

    function startSession($username) {
        $time = time();
        $session_key = md5($this->encodePassword($time . $username . 'key'));
        $this->db->querySafe(
            "UPDATE users SET session_key='%s', last_login='%d' WHERE username='%s'",
            array($session_key, $time, $username)
        );
        return $session_key;
    }

    function encodePassword($password) {
        return hash('sha512', $password);
    }

    function generateRandomPassword($password_length) {
        $alphabet = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789';
        $password = '';
        $length = strlen($alphabet) - 1;
        for ($i = 1; $i <= $password_length; $i += 1) {
            $password .= $alphabet[rand(0, $length)];
        }
        return $password;
    }

    function getBySessionKey($session_key){
        $since = time() - self::SESSION_KEY_EXPIRES_IN;
        $query = $this->db->querySafe(
            "SELECT * FROM users WHERE session_key = '%s' AND last_login > '%d'",
            array($session_key, $since)
        );
        if ($this->db->numRows($query)) {
            return $this->db->fetchSingle($query);
        } else {
            return false;
        }
    }
}
