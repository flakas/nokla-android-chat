<?php

class User_controller extends Controller {

    function __construct() {
        $this->load->model("User");
    }

    function index() {
    }

    function login() {
        $response = array();
        $username = $_POST['username'];
        $password = $_POST['password'];
        if (empty($username) || empty($password)) {
            $response['error'] = 'USER_DOES_NOT_EXIST';
        } elseif (!$this->User->userExists($username, $password)) {
            $response['error'] = 'INCORRECT_USER_CREDENTIALS';
        } else {
            $key = $this->User->startSession($username);
            $user = $this->User->getBySessionKey($key);
            $response['session_key'] = $key;
            $response['success'] = 'USER_LOGGED_IN';
            $response['role'] = $user['role'];
        }
        $this->load->view('Other/json', array('response' => $response));
    }

    function create() {
        $response = array();
        $user = array();
        $error = 0;
        foreach (array('firstname', 'lastname', 'username', 'email') as $key) {
            $user[$key] = $_POST[$key];
            if (empty($user[$key])) {
                $error = 1;
                $response['error'] = 'FIELD_MUST_NOT_BE_EMPTY';
                $response['field'] = $key;
                break;
            }
        }
        if (isset($_POST['role'])) {
            $user['role'] = (int) $_POST['role'];
        } else {
            $user['role'] = 0;
        }
        if (!$error) {
            if ($this->User->userExists($user['username'])) {
                $response['error'] = 'USERNAME_ALREADY_TAKEN';
            } else {
                $password = $this->User->createUser($user);
                $response['password'] = $password;
                $response['success'] = 'USER_CREATED';
            }
        }
        $this->load->view('Other/json', array('response' => $response));
    }

}
