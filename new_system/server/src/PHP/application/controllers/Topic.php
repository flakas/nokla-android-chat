<?php

class Topic_controller extends Controller {

    function __construct() {
        $this->load->model("Topic");
        $this->load->model("User");
    }

    function index() {
    }

    function create(){
        $response = array();
        $topic = array(
            'name' => $_POST['name'],
            'parent' => $_POST['parent'],
            'subtopic' => $_POST['subtopic'],
            'priority' => $_POST['priority']
        );

        if (empty($topic['name'])) {
            $response["error"] = 'EMPTY_TOPIC_NAME';
        } elseif (!($topic['author'] = $this->User->getBySessionKey($_POST['session_key']))) {
            $response["error"] = 'INVALID_SESSION_KEY';
        } elseif (!$this->Topic->isTopicValid(
                $topic['name'],
                $topic['parent'],
                $topic['subtopic'])) {
            $response["error"] = 'TOPIC_ALREADY_EXISTS';
        } else {
            $this->Topic->createTopic($topic);
            $response['success'] = 'TOPIC_CREATED';
        }
        $this->load->view('Other/json', array('response' => $response));
    }

    function with_parent($id = 0) {
        $id = (int) $id;
        $response = array();
        $response['topics'] = array();
        if (!$this->Topic->exists($id)) {
            $response['error'] = 'TOPIC_DOES_NOT_EXIST';
        } else {
            $topics = $this->Topic->getListByParent($id);
            foreach ($topics as $topic) {
                $response['topics'][] = array(
                    'id' => $topic['id'],
                    'name' => $topic['name'],
                    'subtopic' => $topic['subtopic'],
                    'priority' => $topic['priority']
                );
            }
        }
        $this->load->view('Other/json', array('response' => $response));
    }
}
