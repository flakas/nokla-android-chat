<?php

class Publicmessage_controller extends Controller {

    function __construct() {
        $this->load->model('Topic');
        $this->load->model('Publicmessage');
        $this->load->model('User');
    }

    function index() {

    }

    function create() {
        $response = array();
        $message = array(
            'topic' => (int) $_POST['topic'],
            'message' => $_POST['message']
        );
        if (empty($message['topic']) || empty($message['message'])) {
            $response['error'] = 'EMPTY_TOPIC_AND_MESSAGE';
        } elseif (!($message['author'] = $this->User->getBySessionKey($_POST['session_key']))) {
            $response['error'] = 'INVALID_SESSION_KEY';
        } elseif (!$this->Topic->exists($message['topic'])) {
            $response['error'] = 'TOPIC_DOES_NOT_EXIST';
        } else {
            $this->Publicmessage->create($message);
            $response['success'] = 'MESSAGE_POSTED';
        }
        $this->load->view('Other/json', array('response' => $response));
    }

    function get_new() {
        $response = array();
        $topic = (int) $_POST['topic'];
        if (isset($_POST['since'])) {
            $since = (int) $_POST['since'];
        } else {
            $since = 0;
        }
        if (!$this->Topic->exists($topic)) {
            $response['error'] = 'TOPIC_DOES_NOT_EXIST';
        } else {
            $response['messages'] = $this->Publicmessage->getNew($topic, $since);
        }
        $this->load->view('Other/json', array('response' => $response));
    }
}
