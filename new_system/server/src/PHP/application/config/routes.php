<?php

$routes = array();

$routes['default_controller'] = 'User';

$routes['error_404'] = 'errors/page_missing';
$routes['error_500'] = 'errors/internal_server_error';

