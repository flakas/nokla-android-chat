-- DB installation file
DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username		VARCHAR(20) NOT NULL,
    password		CHAR(128) NOT NULL,
    role			SMALLINT(3) NOT NULL,
    email			VARCHAR(100) NOT NULL,
    firstname		VARCHAR(20),
    lastname		VARCHAR(20)
) ENGINE=MyISAM;

DROP TABLE IF EXISTS topics;
CREATE TABLE topics (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name			VARCHAR(50) NOT NULL,
    parent			INT,
    author			INT NOT NULL,
    subtopic 		TINYINT(1),
    priority		SMALLINT(3) NOT NULL DEFAULT '2',
    FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (parent) REFERENCES topics(id) ON DELETE CASCADE
) ENGINE=MyISAM;

DROP TABLE IF EXISTS private_messages;
CREATE TABLE private_messages (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    recipient		INT NOT NULL,
    sender			INT NOT NULL,
    message			TEXT,
    timestamp			INT NOT NULL,
    FOREIGN KEY (recipient) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (sender) REFERENCES users(id) ON DELETE CASCADE
) ENGINE=MyISAM;

DROP TABLE IF EXISTS public_messages;
CREATE TABLE public_messages (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    author			INT NOT NULL,
    timestamp       INT(15) NOT NULL,
    message			TEXT,
    topic			INT NOT NULL,	
    FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (topic) REFERENCES topics(id) ON DELETE CASCADE
) ENGINE=MyISAM;

DROP TABLE IF EXISTS pictures;
CREATE TABLE pictures (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    author			INT NOT NULL,
    timestamp		INT(15) NOT NULL,
    location		VARCHAR(1000),
    topic			INT NOT NULL,	
    FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (topic) REFERENCES topics(id) ON DELETE CASCADE
) ENGINE=MyISAM;
