# Update the repository and install all upgrades
execute "update apt" do
    command "sudo apt-get update && sudo apt-get upgrade -y"
    action :run
end

package 'apache2'
package 'php5'
package 'libapache2-mod-php5'
package 'mysql-server'
package 'libapache2-mod-auth-mysql'
package 'php5-mysql'

service "apache2" do
    action :nothing
end


execute 'create app db' do
    user "root"
    command %{mysql -u root -e 'CREATE DATABASE IF NOT EXISTS pokprog'}
    action :run
end

execute 'soft link' do
    user "root"
    command %{sudo ln -s /vagrant/src/PHP /var/www/pokprog}
    action :nothing
    notifies :restart, "service[apache2]"
end

execute 'change apache vhost' do
    user "root"
    cwd '/etc/apache2/sites-available'
    command %{sudo cp /vagrant/src/DOC/default .}
    action :run
    notifies :restart, "service[apache2]"
end

execute 'enable mod_rewrite' do
    user "root"
    command %{sudo a2enmod rewrite}
    action :run
    notifies :restart, "service[apache2]"
end

execute 'install sql' do
    user "root"
    cwd '/vagrant/src/DOC'
    command %{sudo sh install.sh}
    action :run
end
