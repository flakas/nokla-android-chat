package lt.NoklaChat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class Networking {
	
	protected Socket sock;
	private BufferedReader sockIn;
	private PrintWriter sockOut;
	
	private boolean connected = false;
	
	public Networking(final String host,final int port, final String charset) throws NetException {
		
				Socket newSock = null;
				try {
					newSock = new Socket(host, port);
				}
				catch (UnknownHostException e) {
					throw new NetException("Create 'Networking', wrong hostname entered exception ("+e.getMessage()+")", e);
				}
				catch (IOException e) {
					throw new NetException("Create 'Networking' IO exception ("+e.getMessage()+")", e);
				}
				try {
					startas(newSock, charset);
				} catch (NetException e) {
					e.printStackTrace();
				}
			}
	public Networking(Socket sock, String charset) throws NetException {
		startas(sock, charset);
	}
	
	private void startas(Socket sock, String charset) throws NetException {
		try {
			sockOut = new PrintWriter(new OutputStreamWriter(sock.getOutputStream(), charset));
			sockIn = new BufferedReader(new InputStreamReader(sock.getInputStream(), charset));
			connected = true;	
		}
		catch (IOException e) {
			throw new NetException("Start 'Networking' IO exception ("+e.getMessage()+")", e);
		}
	}
	
	public void disconnect() throws NetException {
		try {
			sockOut.close();
			sockIn.close();
			if (sock != null) {
				sock.close();
			}
			connected = false;
		}
		catch (IOException e) {
			throw new NetException("Disconnect in 'Networking' IO exception ("+e.getMessage()+")", e);
		}
	}
	
	public void send(final String packet) throws NetException {
			sockOut.print(packet+"\r\n");
			if (sockOut.checkError()) {
					throw new NetException("Send in 'Networking' error");
			}
		}	

	public String recv() throws NetException {
		String line;
		
		try {
			line = sockIn.readLine(); 
		}
		catch (IOException e) {
			throw new NetException("Recv in 'Networking' IO exception ("+e.getMessage()+")", e);
		}
		
		if (line == null) {
			throw new NetException("'Networking' lost connection");
		}
		else {
			return line;
		}
		
	}
	
	public boolean isConnected() {
		return connected;
	}
	
}