package lt.NoklaChat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends Activity {

	public static final String VARDAS = null;
	public static final String SLAPTAZODIS = null;
	
	Connectivity inet = new Connectivity(); //Interneto rysio tikrinimui

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}
	
	public void login(View view) {

		EditText editText1 = (EditText) findViewById(R.id.editText1);
		EditText editText2 = (EditText) findViewById(R.id.editText2);
		
		String vardas = editText1.getText().toString();
		if (vardas.isEmpty()) {
			Toast.makeText(getApplicationContext(),"Vardas laukas tu��ias", Toast.LENGTH_SHORT).show();
			return;
		}
		String slaptazodis = editText2.getText().toString();
		if (slaptazodis.isEmpty()) {
			Toast.makeText(getApplicationContext(),"Slapta�od�io laukas tu��ias", Toast.LENGTH_SHORT).show();
			return;
		}
		if (inet.isNetworkAvailable(this)) {
			Intent MainIntent = new Intent(this,MainActivity.class);
			MainIntent.putExtra("VARDAS",vardas);
			MainIntent.putExtra("SLAPTAZODIS",slaptazodis);
			startActivity(MainIntent);
		}
		else {
			Toast.makeText(getApplicationContext(),"Nera interneto rysio", Toast.LENGTH_SHORT).show();
			return;
		}
	}
}
