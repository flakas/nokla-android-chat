package lt.NoklaChat;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;



public class MainActivity extends Activity {
	public String vardas, slaptazodis, data, temp, sessionKey;
	protected EditText msgText;
	public Networking net;
	public JSONObject json;
	public Message msg;
	static ListView msgView;
	public static ArrayAdapter<String> msgList;
	private static Handler handler = new Handler();
	Connectivity inet = new Connectivity(); //Interneto rysio tikrinimui
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		vardas = getIntent().getExtras().getString("VARDAS");
		slaptazodis = getIntent().getExtras().getString("SLAPTAZODIS");
		msgText=(EditText)findViewById(R.id.editText1);
		msgView = (ListView) findViewById(R.id.listView1);

		msgList = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1);

		connect(); //LETS GO
		
		//getActionBar().setDisplayHomeAsUpEnabled(true); Back button virsuje
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			if (net != null) {
				net.disconnect();
			}	
		} 
		catch (NetException e) {
			Log.d("BackToLogin", "Disconnect exception");
		}
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		if (net != null) {
			try {
				net.disconnect();
			} 
			catch (NetException e) {
				Log.d("BackToLogin", "Disconnect exception");
			}
		}	
	}
	
	public static void createMessageReceiverThread(Networking netw) {
		final Networking net = netw;
		(new Thread() {
			
			public void run() {
				String data;
				JSONObject json;
				Message msg;
				
				try {
					for (;;) {
						data = net.recv();
						json = new JSONObject(data);
						
						if (json.getString("action").equals("message")) {
							msg = new Message(data);
							Write("<"+msg.getSender()+"> "+msg.getBody());
						}
					}
				}
				catch (NetException e) {
					Log.d("NetException", "Exception: "+e.getMessage());
				}
				catch (MessageException e) {
					Log.d("MessageException", "Exception: "+e.getMessage());
				}
				catch (JSONException e) {
					Log.d("JSONException", "Exception: "+e.getMessage());
				}
			}
		}).start();	
	}
	
	public static void Write(final String msg) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				msgList.add(msg);
				msgView.setAdapter(msgList);
				msgView.smoothScrollToPosition(msgList.getCount() - 1);
			}
		});		
	}
	
	public void connect() {
		(new Thread() {
			String host = "pi.ign.lt";
			int port = 55555;
			public void run() {
				try {
					net = new Networking(host, port, "UTF-8");
					json = new JSONObject();
					json.put("action", "login");
					json.put("username", vardas);
					json.put("password", slaptazodis);
					net.send(json.toString());
					data = net.recv();
					json = new JSONObject(data);

					if (json.getString("action").equals("login")) {
						temp = json.getString("status");
						if (temp.equals("failed")) {
							net.disconnect();
							Write("Blogas prisijungimo vardas ir/arba slaptazodis");
							
						} else if (temp.equals("success")) {
							Write("Sekmingai prisijungta prie \n"+host+":"+port);
							sessionKey = json.getString("session");
							createMessageReceiverThread(net);
						}
						else if (temp.equals("duplicate")) {
							Write("Toks vartotojas jau prisijunges");
						}
						else {
							Write("Atsitiko kazkas netiketo");
							net.disconnect();
						}
					}
					else {
						Write("Atsitiko kazkas netiketo");
						net.disconnect();
					}
				}
				catch (NetException e) {
					Write(e.getMessage());
					Log.d("NetException", "Exception: "+e.getMessage());
				}
				catch (JSONException e) {
					Log.d("JSONException", "Exception: "+e.getMessage());
				}
			}
		}).start();	
	}

	public void closeKeyboard() {
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE); 

		inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                   InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	public void send(View view) {
		try {
			if (inet.isNetworkAvailable(this) && (!(net == null))) {
				msg = new Message();
				if (!(msgText.getText().toString().isEmpty())){
					msg.setBody(msgText.getText().toString());
					json = new JSONObject(msg.toJSONString());
					json.put("session", sessionKey);
					net.send(json.toString());
					msgText.setText("");
				}
			}
			else {
				Toast.makeText(getApplicationContext(),"Nera interneto rysio arba nepavyko prisijungi prie serverio", Toast.LENGTH_SHORT).show();
				if (net != null) {
					net.disconnect();
				}
				finish(); // uzdarome sita activity
			}
		}
		catch (NetException e) {
			Log.d("NetException", "Exception: "+e.getMessage());
		}
		catch (JSONException e) {
			Log.d("JSONException", "Exception: "+e.getMessage());
		}
		catch (MessageException e) {
			Log.d("MessageException", "Exception: "+e.getMessage());
		}
		closeKeyboard();
	}
}
