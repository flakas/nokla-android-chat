package lt.NoklaChat;
 
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
 
public class Connectivity{
 
    public Boolean isNetworkAvailable(Context con){
    	ConnectivityManager connectivityManager = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    	return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting(); 
    }
}
