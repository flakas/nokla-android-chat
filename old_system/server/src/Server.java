import java.util.ArrayList;

public class Server {
	
	public static ArrayList<ClientSession> sessions = new ArrayList<>();
	
	public static ClientSession getSession(String sessionKey) {
		int index;
		int size = sessions.size();
		boolean success = false;
		
		ClientSession session = null;
		
		for (index=0; index<size; index++) {
            if (sessions.get(index).getSessionKey().equals(sessionKey)) {
                success = true;
                break;
            }
		}
		
		if (success) {
			session = sessions.get(index);
		}
		
		return session;
	}
	
	public static void destroySession(String sessionKey) {
		ClientSession session;
		
		session = Server.getSession(sessionKey);
		
		try {
			session.getNet().disconnect();
			Server.sessions.remove(session);
		}
		catch (NetException e) {
			System.out.println("NetException while removing session: "+e.getMessage());
		}
	}
	
	public static void main(String args[]) {
		NetListener listener;
		Networking newNet;
		
		try {
			listener = new NetListener(55555, "UTF-8");
			
			for (;;) {
				newNet = listener.waitForClient();
				if (newNet == null) {
					break; // listener buvo uždarytas
				}
				else {
					(new ServerWorker(newNet)).start();
				}
			}
			
			//listener.close();
		}
		catch (NetException e) {
			System.out.println("Exception: "+e.getMessage());
			System.exit(1);
		}
	
	}
	
}

