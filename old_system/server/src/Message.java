import org.json.*;

public class Message {
	
	private String body = null;
	private String sender = null;
	private long timeStamp = 0;
	private int rating = 0;
	
	public Message() {
	}
	
	public Message(String JSONString) throws MessageException {
		JSONObject json;
		
		try {
			json = new JSONObject(JSONString);
			
			body = json.getString("body");
		}
		catch (JSONException e) {
			throw new MessageException("Badly formatted JSON ("+e.getMessage()+")", e);
		}
		
		sender = json.optString("sender", null);
		
		timeStamp = json.optLong("timestamp");
		if (timeStamp == 0) {
			timeStamp = System.currentTimeMillis();
		}
		
		rating = json.optInt("rating", 0);
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	public String getBody() {
		return body;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getSender() {
		return sender;
	}
	
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getRating() {
		return rating;
	}
	
	public String toJSONString() throws MessageException {
		JSONObject json;
		
		try {
			json = new JSONObject();
			
			json.put("action", "message");
			
			if (sender != null) {
				json.put("sender", sender);
			}
			if (body != null) {
				json.put("body", body);
			}
			if (timeStamp != 0) {
				json.put("timestamp", timeStamp);
			}
			if (rating != 0) {
				json.put("rating", rating);
			}
			
			return json.toString();
		}
		catch (JSONException e) {
			throw new MessageException("Unexpected exception when making JSON ("+e.getMessage()+")", e);
		}
	}
	
}