import org.json.*;

public class Client {

	public static void createMessageReceiverThread(Networking netw) {
		final Networking net = netw;
		(new Thread() {
			public void run() {
				String data;
				JSONObject json;
				Message msg;
				
				try {
					for (;;) {
						data = net.recv();
						json = new JSONObject(data);
						
						if (json.getString("action").equals("message")) {
							msg = new Message(data);
							
							System.out.println("<"+msg.getSender()+"> "+msg.getBody());
						}
					}
				}
				catch (NetException e) {
					System.out.println("Exception: "+e.getMessage());
				}
				catch (MessageException e) {
					System.out.println("Exception: "+e.getMessage());
				}
				catch (JSONException e) {
					System.out.println("Exception: "+e.getMessage());
				}
			}
		}).start();
	}
	
	public static void main(String args[]) {
		Networking net;
		String sessionKey;
		JSONObject json;
		Message msg;
		String data, temp;
		
		if (args.length != 2) {
			System.out.println("Blogas param skaicius. params: username password");
			System.exit(0);
		}
		
		try {
			net = new Networking("pi.ign.lt", 55555, "UTF-8");
			
			json = new JSONObject();
			json.put("action", "login");
			json.put("username", args[0]);
			json.put("password", args[1]);
			
			net.send(json.toString());
			
			data = net.recv();
			json = new JSONObject(data);
			
			if (json.getString("action").equals("login")) {
				temp = json.getString("status");
				if (temp.equals("failed")) {
					net.disconnect();
					System.out.println("Blogas prisijungimo vardas ir/arba slaptazodis");
				} else if (temp.equals("duplicate")) {
					System.out.println("Toks vartotojas jau prisijunges");
				} else if (temp.equals("success")) {
					sessionKey = json.getString("session");
					
					createMessageReceiverThread(net);
					
					// siunciam 20 feikiniu zinuciu kas 5sec,
					// nes is konsoles negalima vienu metu rasyt ir skaityt,
					// o rasytoja jau turim.
					for (int i=0; i<20; i++) {
						try {
							Thread.sleep(5000);
						}
						catch (InterruptedException e) {
							// dont fakin care
						}
						msg = new Message();
						msg.setBody("random message of "+args[0]+", timestamp "+System.currentTimeMillis());
						json = new JSONObject(msg.toJSONString());
						json.put("session", sessionKey);
						net.send(json.toString());
					}
					
					net.disconnect();
				}
				else {
					System.out.println("Atsitiko kazkas netiketo");
					net.disconnect();
				}
			}
			else {
				System.out.println("Atsitiko kazkas netiketo");
				net.disconnect();
			}
		}
		catch (NetException e) {
			System.out.println("Exception: "+e.getMessage());
		}
		catch (MessageException e) {
			System.out.println("Exception: "+e.getMessage());
		}
		catch (JSONException e) {
			System.out.println("Exception: "+e.getMessage());
		}
		
	}
	
}
