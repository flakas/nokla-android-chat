import java.net.*;
import java.io.*;

public class NetListener {
	
	private ServerSocket servSock;
	private String charset;
	
	public NetListener(int listenPort, String charset) throws NetException {
		try {
			servSock = new ServerSocket(listenPort);
		}
		catch (IOException e) {
			throw new NetException("Create 'NetListener' IO exception ("+e.getMessage()+")", e);
		}
		
		if (charset == null) {
			this.charset = "UTF-16";
		}
		else {
			this.charset = charset;
		}
	}
	
	public Networking waitForClient() throws NetException {
		try {
			return new Networking(servSock.accept(), charset);
		}
		catch (SocketException e) {
			return null;
		}
		catch (IOException e) {
			throw new NetException("'waitForClient' in 'NetListener' IO exception ("+e.getMessage()+")", e);
		}
	}
	
	public void close() throws NetException {
		try {
			servSock.close();
		}
		catch (IOException e) {
			throw new NetException("Clse 'NetListener' IO exception ("+e.getMessage()+")", e);
		}
	}
	
}
