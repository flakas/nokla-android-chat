public class ClientSession {

	private String clientName;
	private String sessionKey;
	private Networking net;
	
	public ClientSession(String clientName, String sessionKey, Networking net) {
		this.clientName = clientName;
		this.sessionKey = sessionKey;
		this.net = net;
	}
	
	public String getClientName() {
		return clientName;
	}
	
	public String getSessionKey() {
		return sessionKey;
	}
	
	public Networking getNet() {
		return net;
	}
	
	public static boolean validUser(String name, String password) {
		if (password.equals("prototipas")) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
