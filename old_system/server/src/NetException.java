public class NetException extends ChatException {
	
	public NetException() {
		super();
	}
	
	public NetException(String message) {
		super(message);
	}
	
	public NetException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
