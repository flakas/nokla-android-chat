import org.json.*;

public class ServerWorker extends Thread {

    Networking net;

    public ServerWorker(Networking net) {
        this.net = net;
    }

    public void run() {

        String data = null;
        JSONObject json;
        String sessionKey = null;
        String username = null;
        Message msg;
        ClientSession sesTemp;
        boolean exists;
        int i, s;
        String action, password, sesKeyTemp;

        System.out.println("New connection established");

        try {

            data = net.recv();

            json = new JSONObject(data);

            action = json.getString("action");

            if (action.equals("login")) {
                username = json.getString("username").trim();
                password = json.getString("password");

                System.out.println("Login. Username: "+username);

                json = new JSONObject();

                json.put("action", "login");

                if (ClientSession.validUser(username, password)) {
                    exists = false;
                    s = Server.sessions.size();
                    for (i=0; i<s; i++) {
                        if (Server.sessions.get(i).getClientName().equals(username)) {
                            exists = true;
                            break;
                        }
                    }

                    if (exists) {
                        System.out.println("Duplicate session attempt. Username: "+username+". New session denied. Disconnecting.");
                        json.put("status", "duplicate");
                        net.send(json.toString());
                        net.disconnect();
                    }
                    else {
                        System.out.println("Login action success. Username: "+username);
                        sessionKey = username+System.currentTimeMillis();
                        json.put("status", "success");
                        json.put("session", sessionKey);
                        Server.sessions.add(new ClientSession(username, sessionKey, net));
                        net.send(json.toString());

                        for (;;) {

                            data = net.recv();

                            json = new JSONObject(data);

                            action = json.getString("action");
                            sesKeyTemp = json.getString("session");

                            sesTemp = Server.getSession(sesKeyTemp);

                            if (sesTemp == null) {
                                System.out.println("Session key mismatch. Username: "+username+". Disconnecting.");
                                Server.destroySession(sessionKey);
                            }
                            else {

                                if (sesTemp.getNet() != net) {
                                    System.out.println("Hack attempt? Username: "+username+". Disconnecting.");
                                    Server.destroySession(sessionKey);
                                    break;
                                }
                                else {

                                    if (action.equals("disconnect")) {
                                        System.out.println("Disconnect action. Username: "+username+". Disconnecting.");
                                        Server.destroySession(sessionKey);
                                        break;
                                    }
                                    else if (action.equals("message")) {
                                        try {
                                            msg = new Message(data);
                                            msg.setBody(msg.getBody().trim());
                                            if (!msg.getBody().equals("")){
                                                msg.setSender(sesTemp.getClientName());
                                                s = Server.sessions.size();
                                                for (i=0; i<s; i++) {
                                                    try {
                                                        sesTemp = Server.sessions.get(i);
                                                        sesTemp.getNet().send(msg.toJSONString());
                                                    }
                                                    catch (NetException e) {
                                                        System.out.println("NetException while relaying message - '"+e.getMessage()+"'. From "+username+" to "+sesTemp.getClientName());
                                                    }
                                                }
                                            }
                                        }
                                        catch (MessageException e) {
                                            System.out.println("Exception on message object - '"+e.getMessage()+"'. Username: "+username);
                                        }
                                    }
                                    else {
                                        System.out.println("Received not supported/implemented action. Username: "+username);
                                    }

                                }

                            }

                        }

                    }
                }
                else {
                    System.out.println("Wrong password/username. Username: "+username+". Disconnecting.");
                    json.put("status", "failed");
                    net.send(json.toString());
                    net.disconnect();
                }
            }
            else {
                System.out.println("First action was not login. Disconnecting.");
                net.disconnect(); // netikėtas "action"
            }

        }
        catch (JSONException e) {
            try {
                if (sessionKey != null) {
                    System.out.println("JSONException in ServerWorker - '"+e.getMessage()+"'. User disconnected: "+Server.getSession(sessionKey).getClientName()+" ("+sessionKey+")");
                    Server.destroySession(sessionKey);
                }
                else if (net.isConnected()) {
                    if (username != null) {
                        System.out.println("JSONException in ServerWorker - '"+e.getMessage()+"'. User disconnected: "+username);
                    }
                    else {
                        System.out.println("JSONException in ServerWorker - '"+e.getMessage()+"'. Connection closed, no user existed");
                    }
                    net.disconnect();
                }
            }
            catch (NetException ex) {
                System.out.println("Serverworker: NetException while disconnecting - '"+ex.getMessage());
            }
        }
        catch (NetException e) {
            try {
                if (sessionKey != null) {
                    System.out.println("User disconnected: "+Server.getSession(sessionKey).getClientName()+" ("+sessionKey+"). "+e.getMessage());
                    Server.destroySession(sessionKey);
                }
                else if (net.isConnected()) {
                    if (username != null) {
                        System.out.println("User disconnected: "+username+". "+e.getMessage());
                    }
                    else {
                        System.out.println("Disconnected: no user had logged in. "+e.getMessage());
                    }
                    net.disconnect();
                }
            }
            catch (NetException ex) {
                System.out.println("Serverworker: NetException while disconnecting - "+ex.getMessage());
            }
        }

    }

}
