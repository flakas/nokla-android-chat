Android chatas
==============

Pirminė versija - Nokla Software

## Atsiskaitymai

- Kovo 20d, 8:30 (7 savaitę)
- Gegužės 15d, 8:30 (14 savaitę)

__Dokumentą siųsti parą prieš atsiskaitymą__

Užduočių sekimui naudojame [https://www.pivotaltracker.com/projects/768335](PivotalTracker)

Diagramas paišome su [http://www.cacoo.com/](Cacoo)

Cacoo diagramų sheet'ų adresai:
[https://cacoo.com/diagrams/Zatuxp2zdBy8cTjY](https://cacoo.com/diagrams/Zatuxp2zdBy8cTjY)
[https://cacoo.com/diagrams/ptNe1tz81Z5xTbvf](https://cacoo.com/diagrams/ptNe1tz81Z5xTbvf)
[https://cacoo.com/diagrams/zOoGlmURBkawCPHN](https://cacoo.com/diagrams/zOoGlmURBkawCPHN)
[https://cacoo.com/diagrams/KBfZUnftucUBDySa](https://cacoo.com/diagrams/KBfZUnftucUBDySa)
