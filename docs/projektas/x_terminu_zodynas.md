# Terminų žodynas

**Administratorius** - sistemos prižiūrėtojas, prižiūrintis sistemos darbą, gali kurti ir naikinti žurnalistų ir redaktorių paskyras;

**Redaktorius** - asmuo, kontroliuojantis ir koordinuojantis žurnalistų darbą, peržiūrintis žurnalistų atsiųstas naujienas ir nuotraukas prieš jų publikavimą, priskiriantis prioritetus temoms;

**Žurnalistas** - asmuo, surenkantis informaciją apie įvykį ir ją persiunčiantis redaktoriui;

**Vartotojas** - sistemos naudotojas: sistemos administratorius, redaktorius arba žurnalistas;

**Serveris** - įrenginys, talpinantis sistemą ir jos duomenis;

**Klientas** - programa, kuri jungiasi per tinklą prie serverio ir siunčia į jį bei gauna iš jo informaciją. Klientas - mobili aplikacija;

**API** - suteikia klientams sąsają su serveriu (angl. Application Programming Interface);

**Prioritetas** - temai redaktoriaus nustatytas svarbos lygis, kuris gali būti „Žemas“, „Vidutinis“ ar „Aukštas“;

**Tema** - kategorija esanti sistemoje, į kurią žurnalistai ir redaktoriai gali rašyti žinutes;

**Potemė** - pokalbių kanalas apie konkretų įvykį, kurį gali kurti ir žurnalistas, ir redaktorius;

**Pastovi tema** -  pokalbių kanalas apie įvykių grupę, kurį gali kurti tik redaktorius;

**Vieša žinutė** - žurnalisto ar redaktoriaus tekstinis pranešimas, kuris yra pridėtas į sistemą konkrečioje temoje;

**Privati žinutė** - žurnalisto ar redaktoriaus žinutė, kurią mato autorius ir gavėjas (žurnalistas ir redaktorius);
