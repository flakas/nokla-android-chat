# Anotacija

Skipbug komanda:

- Laurynas Ginkus
    * El.paštas: laurynas.ginkus@outlook.com
    * Indėlis į darbą: 23%
- Džiugas Pocius
    * El.paštas: dziugas.pocius@gmail.com
    * Indėlis į darbą: 27%
- Tautvidas Sipavičius (komandos lyderis)
    * El.paštas: tautvidas@tautvidas.com
    * Indėlis į darbą: 28%
- Martynas Šalkauskas
    * El.paštas: martynasalk@gmail.com
    * Indėlis į darbą: 22%

# Įvadas

Šiame darbe aprašomas naujienų agentūros jau turimos vidinės žinių perdavimo sistemos, realizuotos kaip Android pokalbių programėlė ir skirtos 
supaprastinti bendravimą tarp žurnalistų ir redaktorių, skatinti jų bendradarbiavimą, žurnalistams operatyviai perduoti naujienas 
iš įvykio vietos į redakciją, kur jos yra peržiūrimos redaktorių ir pranešamos per žinias arba išspausdinamos laikraštyje, tobulinimas.

