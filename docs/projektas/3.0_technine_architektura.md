# Techninė architektūra

\begin{figure}[!ht]
    \centering
    \includegraphics[width=1.00\textwidth]{projektas/img/komponentu.png}
    \caption{Komponentų diagrama}
\end{figure}

\FloatBarrier

**API** - suteikia klientams sąsają su serveriu.

**Messages** - kontroliuoja viešų ir privačių žinučių informaciją.

**Pictures** - atsakingas už vartotojo nuotraukų ir jų informacijos saugojimą.

**Topics** - kontroliuoja temas, atsakingas už jų kūrimą, keitimą, naikinimą.

**Users** - kontroliuoja vartotojų paskyras ir valdo jų informaciją bei autentifikaciją.

**Models (Serverio)** - tarpininkauja tarp valdiklių ir duomenų bazės, apibrėžia metodus užklausų duomenų bazės formavimui.

**Models (Kliento)** - suteikia kliento valdikliams sąsaja su serverio API: paima, išsaugo, apdoroja duomenis.

**User interface** - parodo valdiklių sugeneruotą turinį kliento įrenginyje.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=1.00\textwidth]{projektas/img/deployment.png}
    \caption{Išdėstymo diagrama}
\end{figure}

\FloatBarrier

## Serverio mašinos techniniai parametrai

Procesorius: Intel® Xeon E3-1240V2, 4x2x3.4 GHz

RAM atmintinė: 16GB ECC DDRIII

Kietasis diskas: RAID1 2x 2TB 7200RPM

Raid masyvas: Integrated SATA3 RAID levels 0/1/5/10

Krepšys: Hotswap

Serverio platforma: Intel Ivy bridge

Srautas: 100Mbps/1Gbps port
